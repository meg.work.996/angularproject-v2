import { Directive, Renderer2, ElementRef, Input, AfterViewChecked } from '@angular/core';

@Directive({
  selector: '[appSetBackground]'
})
export class SetBackgroundDirective implements AfterViewChecked {

  link = '';

  @Input() id: string;

  ngAfterViewChecked() {
    this.link = `https://img.youtube.com/vi/${this.id}/maxresdefault.jpg`;
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-image', `url('${this.link}')`);
  }

  constructor(
    private renderer: Renderer2,
    private elementRef: ElementRef
  ) {}

}
