import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'square',
    pathMatch: 'full'
  },
  {
    path: 'square',
    loadChildren: () => import('./modules/square/square.module').then(mod => mod.SquareModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
