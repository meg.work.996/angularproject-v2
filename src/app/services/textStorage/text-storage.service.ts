import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TextStorageService {
  static subject: Subject<string>; // Объект для наблюдения, в данном случае представляет собой строку
  static text: string; // Хранит значение передаваемой строки

  constructor() {
    /**
     * Проверяет, существует ли объект класса subject
     */
    if (!TextStorageService.subject) {
      TextStorageService.subject = new Subject<string>();
    }
  }

  /**
   * Обновляет значения статического свойства static
   * @param text новая строка
   */
  updateText(text: string) {
    TextStorageService.text = text;
  }

  /**
   * Получить объект Observable для наблюдения
   */
  onUpdateText(): Observable<string> {
    return TextStorageService.subject.asObservable();
  }

  /**
   * Запускает "Событие"
   */
  sendText() {
    TextStorageService.subject.next(TextStorageService.text);
  }
}
