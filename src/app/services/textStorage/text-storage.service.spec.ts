import { TestBed } from '@angular/core/testing';

import { TextStorageService } from './text-storage.service';

describe('TextStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TextStorageService = TestBed.get(TextStorageService);
    expect(service).toBeTruthy();
  });
});
