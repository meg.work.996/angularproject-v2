import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {SendMethod} from '../../modules/http/components/http/http.component';
import {catchError, retry, timeout} from 'rxjs/operators';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {
  type: SendMethod; // Хранит тип запроса (get, post)
  data: string; // Передаваемые данные
  requestString: string; // Строка get-запроса
  server = 'https://scloud.ru/user_panel/test_angular.php?method='; // URI сервера

  constructor(private http: HttpClient) {
  }

  doRequest(methodName: string, type: SendMethod, data: string) {
    let params = new HttpParams(); // Тело post-запроса

    /**
     * Если get-запрос
     */
    if (type === 'GET') {
      this.data = JSON.parse(data);
      this.requestString = this.server + methodName + '&';

      /**
       * Формурует строку get-запроса
       */
      for (const key of Object.keys(this.data)) {
        this.requestString += key + '=' + this.data[key] + '&';
      }
      this.requestString = this.requestString.substring(0, this.requestString.length - 1);

      /**
       * Вернуть observable, сделать 3 попытки
       */
      return this.http.get(this.requestString, {observe: 'response'}).pipe(
        timeout(20000),
        retry(2),
        catchError((error: any, caught: any): any => {
          return ErrorObservable.create('error');
        })
      );

      /**
       * Если post-запрос
       */
    } else {
      this.data = JSON.parse(data);

      /**
       * Добавить поля в тело запроса
       */
      for (const key of Object.keys(this.data)) {
        params = params.append(key, this.data[key]);
      }

      /**
       * Вернуть observable, сделать 3 попытки
       */
      return this.http.post('https://scloud.ru/user_panel/test_angular.php?method=' + methodName, params, {observe: 'response'}).pipe(
        timeout(20000),
        retry(2),
        catchError((error: any, caught: any) => {
          return ErrorObservable.create('error');
        })
      );
    }
  }
}
