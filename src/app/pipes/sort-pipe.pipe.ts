import {Pipe, PipeTransform} from '@angular/core';
import {SortDirection} from '../modules/learn-pipes/components/learn-pipes-comp/learn-pipes-comp.component';

@Pipe({
  name: 'sortPipe'
})
export class SortPipePipe implements PipeTransform {

  transform(value: number[][], direction: SortDirection, column: number): any {
    /**
     * Если массив только сгенерировался, возвращает его же
     */
    if (direction === 'unSorted') {
      return value;
    }

    const sortingArray = []; // Содержит массив сортируемых массивов
    /**
     * Формирует массив сортируеых значений
     */
    for (let i = 1; i < value.length; i++) {
      sortingArray[i - 1] = [];
      for (let k = 1; k < value[i].length; k++) {
        sortingArray[i - 1].push(value[i][k]);
      }
    }

    /**
     * Сортировка
     */
    if (direction === 'toBottom') {
      sortingArray.sort((a: number[], b: number[]) => {
        if (a[column] > b[column]) {
          return 1;
        } else if (a[column] === b[column]) {
          return 0;
        } else {
          return -1;
        }
      });
    } else {
      sortingArray.sort((a: number[], b: number[]) => {
        if (a[column] > b[column]) {
          return -1;
        } else if (a[column] === b[column]) {
          return 0;
        } else {
          return 1;
        }
      });
    }

    /**
     * Вписываем отсортированный массив в исходный
     */
    for (let i = 1; i < value.length; i++) {
      for (let k = 1; k < value[i].length; k++) {
        value[i][k] = sortingArray[i - 1][k - 1];
      }
    }


    return value;
  }

}
