import {Pipe, PipeTransform} from '@angular/core';
import {PeriodObject} from '../modules/credit/components/credit/credit.component';

@Pipe({
  name: 'filterCreditResults'
})
export class FilterCreditResultsPipe implements PipeTransform {

  arrayToShow: PeriodObject[];

  transform(value: PeriodObject[], minValue: number, maxValue: number): any {
    this.arrayToShow = [];
    value.forEach((elem) => {
      if (elem.month >= minValue && elem.month <= maxValue) {
        this.arrayToShow.push(elem);
      }
    });
    return this.arrayToShow;
  }

}
