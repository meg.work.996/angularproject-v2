import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(value: number | string, minValue: number, maxValue: number, ...args: any[]): any {
    /**
     * Если значения переменных не входят в допустимый диапазон, устанавливает в них пустую строку и возвращает его
     * Иначе просто возвращает значение
     */
    if (value < minValue || value > maxValue) {
      value = '';
      return value;
    }
    return value;
  }

}
