import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LearnPipesCompComponent} from './components/learn-pipes-comp/learn-pipes-comp.component';


const routes: Routes = [
  {
    path: '',
    component: LearnPipesCompComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearnPipesRoutingModule {
}
