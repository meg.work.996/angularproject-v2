import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {LearnPipesRoutingModule} from './learn-pipes-routing.module';
import {LearnPipesCompComponent} from './components/learn-pipes-comp/learn-pipes-comp.component';
import {Ng5SliderModule} from 'ng5-slider';
import {IgxSliderModule} from 'igniteui-angular';
import {MyPipePipe} from '../../pipes/my-pipe.pipe';
import {SortPipePipe} from '../../pipes/sort-pipe.pipe';


@NgModule({
  declarations: [
    LearnPipesCompComponent,
    MyPipePipe,
    SortPipePipe
  ],
  imports: [
    CommonModule,
    LearnPipesRoutingModule,
    FormsModule,
    Ng5SliderModule,
    IgxSliderModule
  ]
})
export class LearnPipesModule {
}
