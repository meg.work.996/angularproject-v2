import {Component, OnInit} from '@angular/core';
import {SliderType} from 'igniteui-angular';

/**
 * Тип для хранения направления сортировки
 */
export type SortDirection = 'toBottom' | 'toTop' | 'unSorted';

export class Range {
  constructor(public upper: number, public lower: number) {
  }
}

@Component({
  selector: 'app-learn-pipes-comp',
  templateUrl: './learn-pipes-comp.component.html',
  styleUrls: ['./learn-pipes-comp.component.scss']
})
export class LearnPipesCompComponent implements OnInit {

  sliderType = SliderType; // Встроенный тип для слайдера
  isBegan = false; // Хранит, была ли сгенерирована таблица
  someNumber: number; // Хранит вводимое пользователем число
  tableArray: any[][] = []; // Хранит двумерный массив случайных чисел
  minValue: number; // Хранит минимальное значение случайных чисел
  maxValue: number; // Хранит максимальное значение случайных чисел
  sortDirection: SortDirection = 'unSorted'; // Хранит направление сортировки
  column = -1; // Хранит порядковый номер сортируемой колонки
  valueRange: Range; // Хранит выбранный пользователем диапазон слайдера

  constructor() {
  }

  ngOnInit() {
  }

  /**
   * Изменяет значения переменных sortDirection и column
   * @param event передается объект события
   */
  sort(event) {
    if (event.target.attributes.sortDirection.value === 'unSorted') {
      event.target.attributes.sortDirection.value = 'toBottom';
      this.sortDirection = event.target.attributes.sortDirection.value;
    } else if (event.target.attributes.sortDirection.value === 'toBottom') {
      event.target.attributes.sortDirection.value = 'toTop';
      this.sortDirection = event.target.attributes.sortDirection.value;
    } else {
      event.target.attributes.sortDirection.value = 'toBottom';
      this.sortDirection = event.target.attributes.sortDirection.value;
    }
    this.column = event.target.attributes.column.value;
  }

  /**
   * Генерирует и записывает в двумерный массив случайные числа в заданном диапазоне
   */
  createTable() {
    this.tableArray = [];
    const input: HTMLInputElement = document.querySelector('.idInput'); // Получает поле для ввода для стилиации
    const table: HTMLTableElement = document.querySelector('.fieldTable'); // Получает таблицу
    table.classList.add('activeField'); // Стилизация таблицы при появлении

    /**
     * Стилизация и завершение генерации, если не введено значения
     */
    if (!this.someNumber) {
      input.classList.add('idInputRejected');
      return;
    }

    this.minValue = 1; // Уставливает минимальное значение для генерации чисел
    this.maxValue = this.someNumber * this.someNumber; // Устанавливает максимальное значение для генерации числе
    this.valueRange = new Range(this.minValue, this.maxValue); // Устанавливает объект Range (встроенный в слайдер)
    this.isBegan = true; // Устанавливает флаг начала генерации

    /**
     * Удаляет поле, если оно уже есть
     */
    if (document.querySelector('.fieldTable').querySelector('tr')) {
      const resultTable = document.querySelector('.fieldTable');
      const trCollection = resultTable.querySelectorAll('tr');
      for (let i = trCollection.length - 1; i >= 0; i--) {
        const tdCollection = trCollection[i].querySelectorAll('td');
        for (let k = tdCollection.length - 1; k >= 0; k--) {
          trCollection[i].removeChild(tdCollection[k]);
        }
      }
    }

    /**
     * Удаляет стилизацию при ошибке, если ее нет
     */
    input.classList.remove('idInputRejected');

    /**
     * Генерирует случайные числа в массив
     * Перебирает внешний массив
     */
    for (let i = 0; i <= this.someNumber; i++) {
      this.tableArray[i] = []; // Инициализирует в элемента внешнего массива вложенный массив
      /**
       * Перебирает вложенный массив
       */
      for (let k = 0; k <= this.someNumber; k++) {
        /**
         * Если элемент угловой, то ячейка массива содержит пустую строку
         */
        if (i === 0 && k === 0) {
          this.tableArray[i].push('');
          /**
           * Если элемент в первой строке, то добавляет буквенную нотацию (как в Excel)
           */
        } else if (i === 0) {
          this.tableArray[i].push(this.convertToNumberingScheme((k)));
          /**
           * Если элемент в первой колонке, то добавляем цифровую (digit) нотацию
           */
        } else if (k === 0) {
          this.tableArray[i].push(i);
          /**
           * В остальные колонки генерирует случайные числа
           */
        } else {
          this.tableArray[i].push(Math.round(Math.random() * (this.maxValue - 1) + 1));
        }
      }
    }
  }

  /**
   * Устанавливает стили, если блок в фокусе
   */
  setInFocus() {
    document.querySelector('.inputIdContainer').classList.add('focused');
  }

  /**
   * Устанавливает стили, если блок расфокусирован
   */
  setOutFocus() {
    document.querySelector('.inputIdContainer').classList.remove('focused');
  }


  convertToNumberingScheme(n: number): string {
    const x = n - 1;
    const radix = x.toString(26);
    const baseCharCode = 'A'.charCodeAt(0);

    const arr = radix.split('');
    const len = arr.length;

    const newArr = arr.map((val: any, i) => {
      val = parseInt(val, 26);

      if ((i === 0) && (len > 1)) {
        val = val - 1;
      }

      return String.fromCharCode(baseCharCode + val);
    });

    return newArr.join('');
  }
}
