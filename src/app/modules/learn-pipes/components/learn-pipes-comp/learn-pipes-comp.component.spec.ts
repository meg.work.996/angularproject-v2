import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnPipesCompComponent } from './learn-pipes-comp.component';

describe('LearnPipesCompComponent', () => {
  let component: LearnPipesCompComponent;
  let fixture: ComponentFixture<LearnPipesCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearnPipesCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnPipesCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
