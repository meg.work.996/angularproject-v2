import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { VideoPageRoutingModule } from './video-page-routing.module';
import { VideoComponent } from './components/video/video.component';
import { MyIfDirective } from '../../directives/myIf/my-if.directive';
import { SetBackgroundDirective } from '../../directives/setBackground/set-background.directive';
import { TextStorageService } from 'src/app/services/textStorage/text-storage.service';

@NgModule({
  declarations: [
    VideoComponent,
    MyIfDirective,
    SetBackgroundDirective
  ],
  imports: [
    CommonModule,
    VideoPageRoutingModule,
    FormsModule
  ],
  providers: [
    TextStorageService
  ]
})
export class VideoPageModule { }
