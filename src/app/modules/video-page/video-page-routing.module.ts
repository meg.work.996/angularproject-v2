import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoComponent } from './components/video/video.component';


const routes: Routes = [
  {
    path: '',
    component: VideoComponent
  },
  {
    path: 'square'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoPageRoutingModule { }
