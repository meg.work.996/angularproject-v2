import { Component } from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent {

  condition = false; // Хранит, показывать ли блок с превью или нет
  inputValue = 'Показать'; // Хранит свойство value элелмента input
  videoId: string; // Хранит ID видео с YouTube

/**
 * Меняет значение value элемента input и значение bool-переменной condition
 */
  toggle() {
    this.condition = !this.condition;
    !this.condition ? this.inputValue = 'Показать' : this.inputValue = 'Скрыть';
  }

  /**
   * Устанавливает стили, если блок в фокусе
   */
  setInFocus() {
    document.querySelector('.inputIdContainer').classList.add('focused');
  }

  /**
   * Устанавливает стили, если блок расфокусирован
   */
  setOutFocus() {
    document.querySelector('.inputIdContainer').classList.remove('focused');
  }

  constructor() { }
}
