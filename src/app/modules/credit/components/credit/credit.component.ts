import {Component, OnInit, AfterViewChecked} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SliderType} from 'igniteui-angular';
import {Range} from '../../../learn-pipes/components/learn-pipes-comp/learn-pipes-comp.component';

export interface PeriodObject {month: number; balance: number; monthlyPayment: number; percents: number; mainPayment: number}

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit, AfterViewChecked {

  /**
   * Объект формы
   */
  myForm = new FormGroup({
    sum: new FormControl('', [Validators.required, this.sumValidator]),
    percent: new FormControl('', [Validators.required, this.percentValidator]),
    years: new FormControl('', [Validators.required, this.yearsValidator])
  });

  sum: number; // Хранит сумму кредита
  percent: number; // Хранит процентную ставку
  years: number; // Хранит срок кредита
  months: number; // Хранит месяцы кредита
  resultArray: PeriodObject[]; // Массив с результатами вычеслений
  isCalculated = false; // false - калькуляция ни разу не производилась, true - была произведена
  sliderType = SliderType; // Переменная для слайдера
  ranges: Range; // Диапазон для слайдера
  minValue: number; // Минимальное значение слайдера
  maxValue: number; // Максимальное значение слайдера

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewChecked(): void {

  }

  /**
   * Производит валидацию поля для суммы кредита
   * @param elem элемент для валидации
   */
  sumValidator(elem: FormControl): { [key: string]: boolean } {
    if (elem.value % 1 !== 0) {
      return {sum: true};
    } else {
      return null;
    }
  }

  /**
   * Производит валидацию поля для процентной ставки
   * @param elem элемент для валидации
   */
  percentValidator(elem: FormControl): { [key: string]: boolean } {
    if (parseInt(elem.value, 10) > 100 ||
      parseInt(elem.value, 10) < 0 ||
      isNaN(parseFloat(elem.value) && +isFinite(elem.value)) ||
      elem.value.match(/[^.\s]*[0-9]*.?[0-9]+%?/) === null) {
      return {percent: true};
    } else {
      return null;
    }
  }

  /**
   * Производит валидацию поля для срока кредита
   * @param elem элемент для валидации
   */
  yearsValidator(elem: FormControl): { [key: string]: boolean } {
    if (elem.value < 1 || elem.value % 1 !== 0) {
      return {years: true};
    } else {
      return null;
    }
  }

  /**
   * Вычисляет ежемесячный платеж
   * @param sum сумма кредита
   * @param percent процентная ставка
   * @param months месяцы
   */
  getMonthlyPayment(sum: number, percent: number, months: number): number {
    const monthlyPercent = percent / 12;
    const annuitetCoef = monthlyPercent * Math.pow(1 + monthlyPercent, months) / (Math.pow(1 + monthlyPercent, months) - 1);
    return annuitetCoef * sum;
  }

  /**
   * Заполняет массив результатов, возвращает void если все прошло хорошо, возвращает null если есть невалидные поля
   */
  calculate() {
    if (this.myForm.controls.sum.invalid || this.myForm.controls.percent.invalid || this.myForm.controls.years.invalid) {
      this.myForm.get('sum').markAsTouched();
      this.myForm.get('percent').markAsTouched();
      this.myForm.get('years').markAsTouched();
      return null;
    }
    this.isCalculated = true;

    this.sum = this.myForm.controls.sum.value;
    this.percent = parseInt(this.myForm.controls.percent.value, 10) / 100;
    this.months = this.myForm.controls.years.value * 12;

    const monthlyPayment = Math.round(this.getMonthlyPayment(this.sum, this.percent, this.months) * 100) / 100;
    let balance = this.sum;

    const resultArray = [];
    let monthCount: number;
    let percents: number;
    let mainPayment: number;
    for (let i = 0; balance > 0; i++) {
      monthCount = i + 1;
      percents = Math.round((balance * this.percent / 12) * 100) / 100;
      mainPayment = Math.round((monthlyPayment - percents) * 100) / 100;
      resultArray[i] = {
        month: monthCount,
        balance: balance,
        monthlyPayment: monthlyPayment,
        percents: percents,
        mainPayment: mainPayment
      };
      balance = Math.round((balance - mainPayment) * 100) / 100;
      if (i === 0) {
        this.minValue = 1;
      }
      this.maxValue = i + 1;
      this.ranges = new Range(this.maxValue, this.minValue);
    }
    console.log(resultArray);
    this.resultArray = resultArray;
  }

  /**
   * Форматирует введеные значения в поле для процентов
   * @param event объект события
   */
  formatPercent(event) {
    let str = event.target.value;
    str = str.match(/[^.\s]*[0-9]*.?[0-9]+%?/);
    if (str === null) {
      return;
    }
    str = str[0];
    if (str.indexOf(',')) {
      str = str.replace(',', '.');
    }
    if (this.myForm.controls.percent.invalid) {
      // event.target.value = '';
      this.myForm.get('percent').setValue('');
    } else {
      // event.target.value = str;
      this.myForm.get('percent').setValue(str);
    }
    if (this.myForm.controls.percent.value.indexOf('%') === -1) {
      this.myForm.get('percent').setValue(str + '%');
      console.log(this.myForm.controls.percent.value);
    }
  }

  /**
   * Открывает и закрывает выпадающий список с годам
   * @param event объект события
   */
  toggleList(event) {
    const optionsCollection = document.querySelectorAll('.selectYears__itm');
    optionsCollection.forEach((element) => {
      element.classList.toggle('hidden');
    });
    const angle = document.querySelector('.fa');
    angle.classList.toggle('fa-angle-down');
    angle.classList.toggle('fa-angle-up');
  }
  /**
   * Устанавливает срок из всплывающего списка
   * @param event объект события
   */
  selectYears(event) {
    const optionsCollection = document.querySelectorAll('.selectYears__itm');
    document.querySelector('.selectYears__chosed').querySelector('.chosedItm').textContent = event.target.textContent;
    document.querySelector('.fa').classList.toggle('fa-angle-down');
    document.querySelector('.fa').classList.toggle('fa-angle-up');
    if (event.target.textContent === 'Другое') {
      document.querySelector('.yearsInputContainer').classList.remove('hidden');
    } else {
      document.querySelector('.yearsInputContainer').classList.add('hidden');
      this.years = event.target.innerText.match(/[0-9]/)[0];
      this.myForm.get('years').setValue(this.years);
    }
    optionsCollection.forEach((element) => {
      element.classList.toggle('hidden');
    });
  }
}
