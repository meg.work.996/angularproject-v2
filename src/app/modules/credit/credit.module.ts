import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {IgxSliderModule} from 'igniteui-angular';
import {FilterCreditResultsPipe} from '../../pipes/filter-credit-results.pipe';
import {CreditRoutingModule} from './credit-routing.module';
import {CreditComponent} from './components/credit/credit.component';


@NgModule({
  declarations: [
    CreditComponent,
    FilterCreditResultsPipe
  ],
  imports: [
    CommonModule,
    CreditRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IgxSliderModule
  ]
})
export class CreditModule {
}
