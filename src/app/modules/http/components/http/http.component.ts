import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpServiceService} from '../../../../services/httpService/http-service.service';

/**
 * Хранит возможные типы запросов
 */
export type SendMethod = 'GET' | 'POST' | 'Выберите метод отправки запроса';

@Component({
  selector: 'app-http',
  templateUrl: './http.component.html',
  styleUrls: ['./http.component.scss']
})
export class HttpComponent implements OnInit {

  /**
   * Объект формы
   */
  httpForm = new FormGroup({
    method: new FormControl('', [Validators.required, Validators.pattern('[a-z,A-Z]+')]),
    jsonField: new FormControl('', [Validators.required, this.jsonFieldValidator]),
    getJsonField: new FormControl({
      value: '',
      disabled: true
    }),
    getResultsField: new FormControl({
      value: '',
      disabled: true
    })
  });

  optionCollection: NodeListOf<HTMLDivElement>; // Хранит опции селекта
  select: HTMLDivElement; // Хранит сам селект
  angle: HTMLElement; // Angle font-awesome
  typeOfRequest: SendMethod = 'Выберите метод отправки запроса'; // Тип запроса
  methodInput: HTMLInputElement; // Input для метода
  isSelectTouched = false; // Был ли затронут селект
  isValid: boolean; // Состояние валидности всей формы
  methodName: string; // Имя метода
  stringToSend: string; // Передаваемые данные
  outputString: string; // Полученные данные
  isError = false; // Произошла ли ошибка
  responseCode: number; // Код ответа
  responseTime: number; // Время ответа
  responseObject: any; // Объект запроса

  constructor(private httpService: HttpServiceService) {
  }

  ngOnInit() {
    /**
     * DOM-элементы
     */
    this.optionCollection = document.querySelectorAll('.selectYears__itm');
    this.angle = document.querySelector('.fa');
    this.select = document.querySelector('.selectYears__chosed');
    this.methodInput = document.querySelector('.formInputField ');
    /**
     * Стилизация при фокусе
     */
    this.methodInput.addEventListener('focus', () => {
      this.methodInput.classList.add('focused');
    });
    this.methodInput.addEventListener('blur', () => {
      this.methodInput.classList.remove('focused');
    });

    this.onChanges();
  }

  /**
   * Открывает/закрывает селект
   */
  private toggleSelect() {
    this.optionCollection.forEach((elem: HTMLDivElement) => {
      elem.classList.toggle('hidden');
    });

    this.angle.classList.toggle('fa-angle-down');
    this.angle.classList.toggle('fa-angle-up');

    this.select.classList.toggle('focused');
    this.isSelectTouched = true;
  }

  /**
   * Устанавливает тип запроса
   * @param event объект события click
   */
  setMethod(event) {
    this.typeOfRequest = event.target.textContent;
    this.toggleSelect();
  }

  /**
   * Привязывает свойства класса к значениям элементов формы
   */
  onChanges() {
    this.httpForm.get('method').valueChanges.subscribe(str => {
      this.methodName = str;
    });
    this.httpForm.get('jsonField').valueChanges.subscribe(str => {
      this.stringToSend = str;
    });
    this.httpForm.get('getJsonField').valueChanges.subscribe(str => {
      this.outputString = str;
    });
  }

  /**
   * Подписывается на запросы
   */
  onSubmit() {
    this.isValid =
      (this.typeOfRequest !== 'Выберите метод отправки запроса')
      && (this.httpForm.get('method').valid)
      && (this.httpForm.get('jsonField').valid);

    /**
     * Проверка формы на валидность
     */
    if (!this.isValid) {
      this.isSelectTouched = true;
      this.httpForm.get('method').markAsTouched();
      return null;
    }

    const resieveTime = new Date();
    /**
     * Подписаться на результат запроса из сервиса
     */
    const subscription = this.httpService.doRequest(this.methodName, this.typeOfRequest, this.stringToSend).subscribe(
      resp => {
        const incomingTime = new Date(); // Время получения результатов
        this.responseObject = resp; // Объект запроса
        this.responseCode = this.responseObject.status; // Код ответа
        this.httpForm.get('getJsonField').setValue(JSON.stringify(this.responseObject.body, null, '\t')); // Вывести принятые данные
        this.responseTime = resieveTime.getTime() - incomingTime.getTime(); // Калькуляция времени ответа
        this.responseTime = this.responseTime / 1000;
        this.responseTime = Math.abs(this.responseTime);

        this.httpForm.get('getResultsField') // Вывести код и время ответа
          .setValue('Код ответа: ' + this.responseObject.status + '\n' + 'Время ответа: ' + this.responseTime + 'с');
        this.isError = false;
      },
      () => {
        const incomingTime = new Date();
        this.isError = true;
        this.responseCode = this.responseObject.status; // Код ответа
        this.responseTime = resieveTime.getSeconds() - incomingTime.getSeconds(); // Калькуляция времени ответа
      }
    );
  }

  /**
   * Валидация поля для ввода json
   * @param elem элемент формы
   */
  jsonFieldValidator(elem: FormControl): { [key: string]: boolean } {
    let object;
    try {
      JSON.parse(elem.value);
    } catch {
      return {jsonField: true};
    }
    object = JSON.parse(elem.value);
    for (const key of Object.keys(object)) {
      if (typeof object[key] === 'object') {
        return {jsonField: true};
      }
    }
    return null;
  }
}
