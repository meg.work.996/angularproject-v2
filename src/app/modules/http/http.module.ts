import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HttpRoutingModule} from './http-routing.module';
import {HttpComponent} from './components/http/http.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [HttpComponent],
  imports: [
    CommonModule,
    HttpRoutingModule,
    ReactiveFormsModule
  ]
})
export class HttpModule {
}
