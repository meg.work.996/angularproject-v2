import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SquareComponent } from './components/square/square.component';


const routes: Routes = [
  {
    path: '',
    component: SquareComponent,
    children: [
      {
        path: 'styles/:width/:height/:backgroundColor/:border/:position/:top/:left/:zIndex/:borderRadius/:boxShadow',
        loadChildren: () => import('../../modules/styles/styles.module').then(mod => mod.StylesModule)
      }
    ]
  },
  {
    path: 'video',
    loadChildren: () => import('../video-page/video-page.module').then(mod => mod.VideoPageModule)
  },
  {
    path: 'pages',
    loadChildren: () => import('../pages/pages.module').then(mod => mod.PagesModule)
  },
  {
    path: 'pipes',
    loadChildren: () => import('../learn-pipes/learn-pipes.module').then(mod => mod.LearnPipesModule)
  },
  {
    path: 'credit',
    loadChildren: () => import('../credit/credit.module').then(mod => mod.CreditModule)
  },
  {
    path: 'http',
    loadChildren: () => import('../http/http.module').then(mod => mod.HttpModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SquareRoutingModule { }
