import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


// Тип для выбора метода транформации
type directionType = 'drag' | 'toLeft' | 'toRight' | 'toTop' | 'toBottom';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {
  direction: directionType; // Содержит направление трансформации
  leftOffset: number; // Отступ слева до края
  topOffset: number; // Отступ сверху до края
  innerX: number; // Отступ внутри от края до места клика по X
  innerY: number; // Отступ внутри от края до места клика по Y
  defaultX: number; // Координата X при начале трансформации
  difference: number; // Величина расстяжения (дельта)
  defaultWidth: number; // Изначальная ширина
  defaultLeftPos: number; // Изначальный стиль left
  defaultTopPos: number; // Изначальный стиль top
  defaultY: number; // Координата Y при начале трансформации
  defaultHeight: number; // Изначальная высота
  styles: object = {};
  /**
   * Задать направление и способ трансформации и управлять курсором
   * @param event параметр для mousemove
   */
  prepareToMove(event): void {
    const square: HTMLElement = event.target;
    if (event.offsetX < 10) {
      square.style.cursor = 'ew-resize';
      this.direction = 'toLeft';
    } else if (event.target.offsetWidth - event.offsetX < 10) {
      square.style.cursor = 'ew-resize';
      this.direction = 'toRight';
    } else if (event.offsetY < 10) {
      square.style.cursor = 'ns-resize';
      this.direction = 'toTop';
    } else if (event.target.offsetHeight - event.offsetY < 10) {
      square.style.cursor = 'ns-resize';
      this.direction = 'toBottom';
    } else {
      square.style.cursor = 'move';
      this.direction = 'drag';
    }
  }


  /**
   * Генерация нужных стилей для отображения
   */
  generate() {
    const square: HTMLElement = document.querySelector('.square');
    const computedStyles: CSSStyleDeclaration = window.getComputedStyle(square);
    const width = 'width';
    this.styles[width] = computedStyles.width;
    const height = 'height';
    this.styles[height] = computedStyles.height;
    const backgroundColor = 'background-color';
    this.styles[backgroundColor] = computedStyles.backgroundColor;
    const border = 'border';
    this.styles[border] = computedStyles.border;
    const position = 'position';
    this.styles[position] = computedStyles.position;
    const top = 'top';
    this.styles[top] = computedStyles.top;
    const left = 'left';
    this.styles[left] = computedStyles.left;
    const zIndex = 'z-index';
    this.styles[zIndex] = computedStyles.zIndex;
    const borderRadius = 'border-radius';
    this.styles[borderRadius] = computedStyles.borderRadius;
    const boxShadow = 'box-shadow';
    this.styles[boxShadow] = computedStyles.boxShadow;
    this.router.navigate(['/square/styles',
      this.styles[width],
      this.styles[height],
      this.styles[backgroundColor],
      this.styles[border],
      this.styles[position],
      this.styles[top],
      this.styles[left],
      this.styles[zIndex],
      this.styles[borderRadius],
      this.styles[boxShadow]
    ]);
  }

  /**
   * Отслеживает клик и начинает транформацию
   * @param event параметр для события mousemove
   */
  startTransform(event, moveTo: directionType): void {
    const square: HTMLElement = event.target;
    this.leftOffset = event.pageX - square.offsetWidth; // Содержит отступ слева
    this.topOffset = event.pageY; // Содержит отступ сверху
    this.innerX = event.offsetX; // Внутренний отступ слева
    this.innerY = event.offsetY; // Внутренний отступ сверху
    this.defaultX = event.pageX; // Координата X на начало трансформации
    this.defaultWidth = square.offsetWidth; // Изначальная ширина
    this.defaultLeftPos = parseInt(square.style.left, 10); // Изначальная позиция по left
    this.defaultTopPos = parseInt(square.style.top, 10); // Изначальная позиция по top
    this.defaultY = event.pageY; // Координата Y на начало трансформации
    this.defaultHeight = square.offsetHeight; // Изначальная высота
    /**
     * Трансформация
     */
    document.onmousemove = (e): void => {
      if (moveTo === 'toRight') {
        this.difference = e.pageX - this.defaultWidth - this.leftOffset;
        square.style.width = this.defaultWidth + this.difference * 2 + 'px';
        square.style.left = this.defaultLeftPos - this.difference + 'px';
        document.onmouseup = (ev): void => {
          document.onmousemove = null;
        };
      } else if (moveTo === 'toBottom') {
        this.difference = e.pageY - this.defaultHeight - this.topOffset + this.defaultHeight;
        square.style.height = this.defaultHeight + this.difference * 2 + 'px';
        square.style.top = this.defaultTopPos - this.difference + 'px';
        document.onmouseup = (ev): void => {
          document.onmousemove = null;
        };
      } else if (moveTo === 'drag') {
        square.style.left = e.pageX - this.innerX + 'px';
        square.style.top = e.pageY - this.innerY + 'px';
        document.onmouseup = (ev): void => {
          document.onmousemove = null;
        };
      } else if (moveTo === 'toTop') {
        this.difference = -(e.pageY - this.topOffset);
        square.style.top = this.defaultTopPos - this.difference + 'px';
        square.style.height = this.defaultHeight + this.difference * 2 + 'px';
        document.onmouseup = (ev): void => {
          document.onmousemove = null;
        };
      } else if (moveTo === 'toLeft') {
        this.difference = -(e.pageX - this.leftOffset - this.defaultWidth);
        square.style.left = this.defaultLeftPos - this.difference + 'px';
        square.style.width = this.defaultWidth + this.difference * 2 + 'px';
        document.onmouseup = (ev): void => {
          document.onmousemove = null;
        };
      }
    };
  }

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
