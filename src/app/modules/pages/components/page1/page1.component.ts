import { Component, OnDestroy } from '@angular/core';
import { TextStorageService } from 'src/app/services/textStorage/text-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnDestroy {
  text: string; // Строка текста
  subscription: Subscription; // Объект подписки

  /**
   * Конструктор Page1Component
   * @param textStorage сервис с передаваемой строкой
   */
  constructor(private textStorage: TextStorageService) {
    /**
     * Подписываемся и указываем, что делать с полученными из сервиса данными
     */
    this.subscription = this.textStorage.onUpdateText().subscribe(text => { this.text = text; });
    /**
     * Запустить "событие"
     */
    this.textStorage.sendText();
  }

  /**
   * Обновляет данные в подключенном сервисе
   */
  sendText() {
    this.textStorage.updateText(this.text);
  }

  /**
   * Отмена подписки при уничтожении компонента
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
