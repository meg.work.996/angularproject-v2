import { Component, OnDestroy } from '@angular/core';
import { TextStorageService } from 'src/app/services/textStorage/text-storage.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnDestroy {
  text: string; // Строка текста
  subscription: Subscription; // Объект подписки

  /**
   * Конструктор класса Page2Component
   * @param textStorage сервис с передаваемыми данными
   */
  constructor(private textStorage: TextStorageService) {
    /**
     * Подписываемся и указываем, что делать с полученными из сервиса данными
     */
    this.subscription = this.textStorage.onUpdateText().subscribe(text => (this.text = text));
    /**
     * Запустить "событие"
     */
    this.textStorage.sendText();
  }

  /**
   * Обновляет данные в подключенном сервисе
   */
  sendText() {
    this.textStorage.updateText(this.text);
  }

  /**
   * Отмена подписки при уничтожении компонента
   */
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
