import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PagesRoutingModule } from './pages-routing.module';
import { Page1Component } from './components/page1/page1.component';
import { Page2Component } from './components/page2/page2.component';


@NgModule({
  declarations: [Page1Component, Page2Component],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule
  ],
  providers: []
})
export class PagesModule { }
