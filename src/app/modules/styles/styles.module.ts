import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StylesRoutingModule } from './styles-routing.module';
import { StylesCompComponent } from './components/styles-comp/styles-comp.component';


@NgModule({
  declarations: [StylesCompComponent],
  imports: [
    CommonModule,
    StylesRoutingModule
  ]
})
export class StylesModule { }
