import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StylesCompComponent } from './styles-comp.component';

describe('StylesCompComponent', () => {
  let component: StylesCompComponent;
  let fixture: ComponentFixture<StylesCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StylesCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StylesCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
