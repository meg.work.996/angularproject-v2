import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-styles-comp',
  templateUrl: './styles-comp.component.html',
  styleUrls: ['./styles-comp.component.scss']
})
export class StylesCompComponent implements OnInit {

  private subscription: Subscription; // Объект подписки
  private width: string; // Ширина
  private height: string; // Высота
  private backgroundColor: string; // Цвет
  private border: string; // Граница
  private position: string; // Позиция
  private top: string; // Позиция по top
  private left: string; // Позиция по bot
  private zIndex: string; // z-index
  private borderRadius: string; // Закругление углов
  private boxShadow: string; // Тень

  constructor(private route: ActivatedRoute) {
    this.subscription = route.params.subscribe(params => this.width = 'width: ' + params['width']);
    this.subscription = route.params.subscribe(params => this.height = 'height: ' + params['height']);
    this.subscription = route.params.subscribe(params => this.backgroundColor = 'background-color: ' + params['backgroundColor']);
    this.subscription = route.params.subscribe(params => this.border = 'border: ' + params['border']);
    this.subscription = route.params.subscribe(params => this.position = 'position: ' + params['position']);
    this.subscription = route.params.subscribe(params => this.top = 'top: ' + params['top']);
    this.subscription = route.params.subscribe(params => this.left = 'left: ' + params['left']);
    this.subscription = route.params.subscribe(params => this.zIndex = 'z-index: ' + params['zIndex']);
    this.subscription = route.params.subscribe(params => this.borderRadius = 'border-radius: ' + params['borderRadius']);
    this.subscription = route.params.subscribe(params => this.boxShadow = 'box-shadow: ' + params['boxShadow']);
  }

  ngOnInit() {
  }

}
