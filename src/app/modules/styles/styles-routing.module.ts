import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StylesCompComponent } from './components/styles-comp/styles-comp.component';


const routes: Routes = [
  {
    path: '',
    component: StylesCompComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StylesRoutingModule { }
